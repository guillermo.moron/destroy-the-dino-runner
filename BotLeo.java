import java.awt.Robot;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;

public class BotLeo {
	
	private Robot robot;
	
	//constructeur du bot
	public BotLeo() {
		try {
			this.robot = new Robot();
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	//fonction de saut du robot
	public void jump() {
		
		//appui, maintien appuyé pendant 0.1s et relache la barre espace
		this.robot.keyPress(KeyEvent.VK_SPACE);
		this.robot.delay(100);
		this.robot.keyRelease(KeyEvent.VK_SPACE);
	}
	

	public static void main(String[] args) {
		
		//creation du bot
		BotLeo leo = new BotLeo();

		//attente de lancement de la boucle pour changer de fenêtre (2 secondes)
		leo.robot.delay(2000);
		
		//debut de jeu
		leo.robot.mouseMove(545,475);
		leo.robot.mousePress(InputEvent.BUTTON1_MASK);
		leo.jump();
		
		//temps de jeu
		long endtime = System.currentTimeMillis() + 150000; //150 secondes
		
		while(endtime > System.currentTimeMillis()) {
			
			//si la couleur d'un des capteurs est de la même couleur qu'un obstacle le dino saute
			if(leo.robot.getPixelColor(545,475).getRed() == 83 || leo.robot.getPixelColor(550,444).getRed() == 83) {
				leo.jump();
			}
		}
		
		
	}
}